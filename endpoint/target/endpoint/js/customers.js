$(document).ready(function(){
    disableButtons()

    $("#plans").click(function() {
        $.redirect('/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#add_new_customer").click(function() {
        $.redirect('/add_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#edit_customer").click(function() {
        var table = $('#customer_list_id').DataTable();
        var rowData = $.map(table.rows('.selected').data(), function (item) {
            return item
        });
        localStorage.setItem("id", rowData[0]);
        $.redirect('/edit_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#delete_customer").click(function() {

    });

    var del = document.createElement('input');
    del.type = 'button';
    del.id = 'btn123';
    del.name = 'delll';
    del.value = 'del';
    del.onclick = function(){
        var table = $('#customer_list_id').DataTable();


        var ids = $.map(table.rows('.selected').data(), function (item) {
            return item
        });
        console.log(ids)
    };

    document.body.appendChild(del);

    function disableButtons() {
        $('#edit_customer').attr('disabled','disabled');
        $('#delete_customer').attr('disabled','disabled');
    }


    function enableButtons() {
        $('#edit_customer').removeAttr('disabled');
        $('#delete_customer').removeAttr('disabled');
    }

    $.get({
            url: 'rest/get_customers',
            headers: {
                'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
            }
        }).done(function(data) {
        $(document).ready(function() {
            var table = $('#customer_list_id').DataTable();

            $('#customer_list_id tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                    disableButtons()
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    enableButtons()
                }
            } );

            $('#button').click( function () {
                table.row('.selected').remove().draw( false );
            } );
        } );


        var json = $.parseJSON(data);
        var dataSet = []
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            var del = document.createElement('input');
            del.type = 'button';
            del.name = 'delll';
            del.value = 'del';
            del.onclick = function () {
                alert("hi  jaavscript");
            };


            dataSet.push([obj.id, obj.firstName, obj.lastName, obj.login, obj.pass, obj.balance])
        }

        //$("#customer_list_id").html(data);
        $('#customer_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { "visible": false, title: "Id"},
                    { title: "Fist Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Pass" },
                    { title: "Balance" },
                ]
            });
    });
});