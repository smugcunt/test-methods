$(document).ready(function(){
    disableButtons()
    $("#customers").click(function() {
        $.redirect('/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#add_new_plan").click(function() {
        $.redirect('/add_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#edit_plan").click(function() {
        var table = $('#plan_list_id').DataTable();
        var rowData = $.map(table.rows('.selected').data(), function (item) {
            return item
        });
        localStorage.setItem("id", rowData[0]);
        $.redirect('/edit_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    $("#delete_plan").click(function() {

    });

    var del = document.createElement('input');
    del.type = 'button';
    del.id = 'btn123';
    del.name = 'delll';
    del.value = 'del';
    del.onclick = function(){
        var table = $('#plan_list_id').DataTable();


        var ids = $.map(table.rows('.selected').data(), function (item) {
            return item
        });
        console.log(ids)
    };

    document.body.appendChild(del);

    function disableButtons() {
        $('#edit_plan').attr('disabled','disabled');
        $('#delete_plan').attr('disabled','disabled');
    }


    function enableButtons() {
        $('#edit_plan').removeAttr('disabled');
        $('#delete_plan').removeAttr('disabled');
    }


    $.get({
            url: 'rest/get_all_plans',
            headers: {
                'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
            }
        }).done(function(data) {
        $(document).ready(function() {
            var table = $('#plan_list_id').DataTable();

            $('#plan_list_id tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                    disableButtons()
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    enableButtons()
                }
            } );

            $('#button').click( function () {
                table.row('.selected').remove().draw( false );
            } );
        } );


        var json = $.parseJSON(data);
        var dataSet = []
        for(var i = 0; i < json.length; i++){
            var obj = json[i];
            dataSet.push([obj.id, obj.name, obj.details, obj.fee])
        }

        //$("#customer_list_id").html(data);
        $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { "visible": false, title: "Id"},
                    { title: "Name" },
                    { title: "Description" },
                    { title: "Fee" },
                ]
            });
    });
});