$(document).ready(function(){
    $.get({
        url: 'rest/get_customer/' + localStorage.getItem("id"),
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
            'Content-Type': 'application/json'
        }
    }).done(function(data) {
        var obj = data[0];
        $("#first_name_id").val(obj.firstName);
        $("#last_name_id").val(obj.lastName);
    });


    $("#edit_customer_id").click(
        function() {
            var fName = $("#first_name_id").val();
            var lName = $("#last_name_id").val();

            if(fName =='' || lName =='') {
                $('input[type="text"],input[type="password"]').css("border","2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                alert("Name, details or fee is empty");
            } else {
                $.post({
                    url: 'rest/update_customer',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "id": localStorage.getItem("id"),
                        "firstName": fName,
                        "lastName": lName
                    })
                }).done(function (data) {
                    $.redirect('/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});