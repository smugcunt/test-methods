$(document).ready(function(){
    $.get({
        url: 'rest/get_plan/' + localStorage.getItem("id"),
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
            'Content-Type': 'application/json'
        }
    }).done(function(data) {
        console.log(data);
        var obj = data[0];
        $("#name_id").val(obj.name);
        $("#details_id").val(obj.details);
        $("#fee_id").val(obj.fee);
    });

    $("#edit_plan_id").click(
        function() {
            var name = $("#name_id").val();
            var details = $("#details_id").val();
            var fee = $("#fee_id").val();

            if(name =='' || details =='' || fee == '') {
                $('input[type="text"],input[type="password"]').css("border","2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                alert("Name, details or fee is empty");
            } else {
                $.post({
                    url: 'rest/update_plan',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                                             "id": localStorage.getItem("id"),
                                         	 "name":name,
                                             "details":details,
                                             "fee":fee,
                                         })
                }).done(function(data) {
                     $.redirect('/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});