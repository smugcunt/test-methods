$(document).ready(function(){
    $("#create_plan_id").click(
        function() {
            var name = $("#name_id").val();
            var details = $("#details_id").val();
            var fee = $("#fee_id").val();

            // check fields
            if(name =='' || details =='' || fee == '') {
                $('input[type="text"],input[type="password"]').css("border","2px solid red");
                $('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
                alert("Name, details or fee is empty");
            } else {
                $.post({
                    url: 'rest/create_plan',
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                                         	 "name":name,
                                             "details":details,
                                             "fee":fee,
                                         })
                }).done(function(data) {
                     $.redirect('/plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});