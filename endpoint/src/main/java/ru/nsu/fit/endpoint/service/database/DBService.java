package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_WITH_NAME_AND_PASSWORD = "SELECT * FROM CUSTOMER WHERE login='%s' AND pass='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_AVAIL_PLANS = "SELECT * FROM PLAN AS P WHERE P.id NOT IN (SELECT plan_id FROM SUBSCRIPTION AS S WHERE S.customer_id='%s') AND P.fee <= (SELECT balance FROM CUSTOMER WHERE id='%s')";
    private static final String SELECT_CUSTOMER_SUBSCRIPTIONS = "SELECT id, name, details, fee FROM SUBSCRIPTION_PLAN WHERE customer_id='%s'";

    private static final String UPDATE_CUSTOMER = "UPDATE CUSTOMER SET first_name='%s', last_name='%s' WHERE id='%s'";
    private static final String UPDATE_PLAN = "UPDATE PLAN SET name='%s', details='%s', fee='%s' WHERE id='%s'";
    private static final String UPDATE_DECREASE_CUSTOMER_BALANCE = "UPDATE CUSTOMER SET BALANCE = BALANCE - (SELECT fee FROM PLAN WHERE id='%s') WHERE id = '%s'";
    private static final String UPDATE_INCREASE_CUSTOMER_BALANCE = "UPDATE CUSTOMER SET BALANCE = BALANCE + %s WHERE id = '%s'";


    private static final String DELETE_CUSTOMER= "DELETE FROM CUSTOMER WHERE id='%s'";
    private static final String DELETE_PlAN = "DELETE FROM PLAN WHERE id='%s'";
    private static final String DELETE_SUBSCRIPTION = "DELETE FROM SUBSCRIPTION WHERE id='%s'";
    private static final String DELETE_SUBSCRIPTIONS_WITH_PLAN = "DELETE FROM SUBSCRIPTION WHERE plan_id='%s'";
    private static final String DELETE_SUBSCRIPTIONS_WITH_CUSTOMER = "DELETE FROM SUBSCRIPTION WHERE customer_id='%s'";



    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    public Customer createCustomer(Customer customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: '%s'", customerData));

            customerData.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.getId(),
                                customerData.getFirstName(),
                                customerData.getLastName(),
                                customerData.getLogin(),
                                customerData.getPass(),
                                customerData.getBalance()));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Customer updateCustomer(Customer customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updateCustomer' was called with data: '%s'", customerData));

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER,
                                customerData.getFirstName(),
                                customerData.getLastName(),
                                customerData.getId()));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public void removeCustomer(UUID customerID) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeCustomer' was called with data: '%s'", customerID));
            try {
                Statement statement = connection.createStatement();

                statement.executeUpdate(
                        String.format(
                                DELETE_SUBSCRIPTIONS_WITH_CUSTOMER,
                                customerID));

                statement.executeUpdate(
                        String.format(
                                DELETE_CUSTOMER,
                                customerID)) ;

            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
    public List<Customer> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer> result = Lists.newArrayList();
                while (rs.next()) {
                    Customer customerData = new Customer()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(rs.getInt(6));

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }



    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Customer checkIsCustomerInDB(String customerLogin, String customerPassword)
    {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'checkIsCustomerInDB' was called with data name='%s' password='%s'.", customerLogin, customerPassword));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_WITH_NAME_AND_PASSWORD,
                                customerLogin, customerPassword));
                if (rs.next()) {
                    return new Customer()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + "' and password='" + customerPassword + "' was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan createPlan(Plan plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", plan));

            plan.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                plan.getId(),
                                plan.getName(),
                                plan.getDetails(),
                                plan.getFee()));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan updatePlan(Plan planData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updatePlan' was called with data: '%s'", planData));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_PLAN,
                                planData.getName(),
                                planData.getDetails(),
                                planData.getFee(),
                                planData.getId()));
                return planData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void removePlan(UUID planID) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removePlan' was called with data: '%s'", planID));
            try {
                Statement statement = connection.createStatement();

                statement.executeUpdate(
                        String.format(
                                DELETE_SUBSCRIPTIONS_WITH_PLAN,
                                planID));

                statement.executeUpdate(
                        String.format(
                                DELETE_PlAN,
                                planID)) ;

            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Plan> getAllPlans() {
        synchronized (generalMutex) {
            logger.info("Method 'getAllPlans' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                List<Plan> result = Lists.newArrayList();
                while (rs.next()) {
                    Plan planData = new Plan()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(Integer.valueOf(rs.getString(4)));

                    result.add(planData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Plan> getAllAvailPlansForCustomer(UUID customerId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getAllAvailPlansForCustomer' was called with data: '%s'", customerId));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_AVAIL_PLANS, customerId.toString(), customerId.toString()));
                List<Plan> result = Lists.newArrayList();
                while (rs.next()) {
                    Plan planData = new Plan()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(Integer.valueOf(rs.getString(4)));
                    result.add(planData);
                }

                return result;
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                System.out.println(ex.getMessage());
                throw new RuntimeException(ex);
            }
        }
    }

    public Subscription createSubscription(Subscription subscription) {
        synchronized (generalMutex) {
            logger.info("Method createSubscription was called",subscription);

            subscription.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();

                // Уменьшаем баланс пользователя на стоимость подписки.
                statement.executeUpdate(
                        String.format(
                                UPDATE_DECREASE_CUSTOMER_BALANCE,
                                subscription.getPlanId(), subscription.getCustomerId()));

                statement.executeUpdate(
                        String.format(
                                INSERT_SUBSCRIPTION,
                                subscription.getId(),
                                subscription.getCustomerId(),
                                subscription.getPlanId()));
                return subscription;

            } catch (SQLException ex){
                logger.error(ex.getMessage(),ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void removeSubscription(UUID subscriptionID) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeSubscription' was called with data: '%s'", subscriptionID));
            try {
                System.out.println(subscriptionID);
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_SUBSCRIPTION,
                                subscriptionID)) ;

            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Plan> getCustomerSubscriptions(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomerSubscriptions' was called with data: '%s'", customerId);

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_CUSTOMER_SUBSCRIPTIONS, customerId));

                List<Plan> result = Lists.newArrayList();
                while (rs.next()) {
                    Plan planData = new Plan();
                    planData.setId(UUID.fromString(rs.getString(1)));
                    planData.setName(rs.getString(2));
                    planData.setDetails(rs.getString(3));
                    planData.setFee(Integer.valueOf(rs.getString(4)));
                    result.add(planData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void topUpBalance(UUID customerId, int amount){
        synchronized (generalMutex) {
            logger.info("Method topUpBalance was called",customerId);
            try {
                System.out.println(customerId);
                System.out.println(amount);
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(
                        UPDATE_INCREASE_CUSTOMER_BALANCE,
                        amount,
                        customerId));
            } catch (SQLException ex){
                logger.error(ex.getMessage(),ex);
                throw new RuntimeException(ex);
            }
        }

    }
    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://d0d12210-3a0d-4031-9ce1-a96500b57f57.mysql.sequelizer.com:3306/dbd0d122103a0d40319ce1a96500b57f57?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "eqmpibwnfgjgbsbj",
                            "dNeirueyb7pPKaGfodzhfZCrAe3xavPJxwSW8bfXGeJuWn5mPqabq4qmjVQmANxN");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
