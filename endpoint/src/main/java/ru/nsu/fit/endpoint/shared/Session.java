package ru.nsu.fit.endpoint.shared;

import ru.nsu.fit.endpoint.service.MainFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Session {
    public static SessionUser User = null;

    public static Boolean UpdateUserData()
    {
        if (User != null)
            if (User.Id != null)
            {
                List<Customer> customers = MainFactory.getInstance().getCustomerManager().getCustomers().stream()
                        .filter(x -> x.getId().equals(Session.User.Id))
                        .collect(Collectors.toList());
                if (customers.size() == 1)
                {
                    Customer c = customers.get(0);
                    User.FirstName = c.getFirstName();
                    User.LastName = c.getLastName();
                    User.Login = c.getLogin();
                    User.Password = c.getPass();
                    User.Balance = c.getBalance();
                    return true;
                }

            }
        return false;

    }


    public static class SessionUser {
        public String UserRole;
        public UUID Id;
        public String FirstName;
        public String LastName;
        public String Login;
        public String Password;
        public int Balance;

        public SessionUser(String userRole, UUID id,String firstName, String lastName, String login, String password){
            this.UserRole = userRole;
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Login = login;
            this.Password = password;
        }


    }
}


