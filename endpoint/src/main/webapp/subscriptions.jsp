<%@ page import="ru.nsu.fit.endpoint.shared.Session" %>
<%@ page import="ru.nsu.fit.endpoint.rest.RestService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Subscriptions</title>
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.redirect.js"></script>
    <script src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/subscriptions.js"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="jumbotron">
        <input id="customer_id" name="hidden_input" type="hidden" value="<%=Session.User.Id%>">
        <% Session.UpdateUserData(); %>
        <h1>Welcome, <%=Session.User.FirstName + " " + Session.User.LastName%></h1>
        <h2>Your balance: <%=Session.User.Balance%></h2>
        <button class="btn btn-dark" id="change_data">Change your data</button>
        <button class="btn btn-dark" id="top_up_balance">Top up the balance</button>
        <br>
        <h2>Your subscriptions</h2>
        <button class="btn btn-dark" id="add_new_subscription">Add subscription</button>
        <button class="btn btn-dark" id="delete_subscription">Delete subscription</button>
        <br><br><br>
        <table id="subscription_list_id" class="display" width="100%"></table>
    </div>

</body>
</html>