$(document).ready(function(){
    localStorage.setItem('userId', $(customer_id).val());

    disableButtons();
    // $("#customers").click(function() {
    //     $.redirect('/customers.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    // });
    $("#add_new_subscription").click(function() {
        $.redirect('/add_subscription.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
    // $("#edit_plan").click(function() {
    //     var table = $('#plan_list_id').DataTable();
    //     var rowData = $.map(table.rows('.selected').data(), function (item) {
    //         return item
    //     });
    //     localStorage.setItem("id", rowData[0]);
    //     $.redirect('/edit_plan.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    // });
    $("#change_data").click(function() {
        $.get({
            url: 'rest/get_customer/' + $(customer_id).val(),
            headers: {
                'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                'Content-Type': 'application/json'
            }
        }).done(function(data) {
            var obj = data[0]
            localStorage.setItem("userId", obj.id);
            localStorage.setItem("type", "customer");
            $.redirect('/edit_customer.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
        });
    });
    $("#top_up_balance").click(function () {
        $.redirect('top_up_balance.jsp')
    });
    $("#delete_subscription").click(function() {
        if (confirm("Do you really want to remove this subscription?")){
             var table = $('#subscription_list_id').DataTable();
             var rowData = $.map(table.rows('.selected').data(), function (item) {
                 return item
             });
             $.post({
                 url: 'rest/delete_subscription/' + rowData[0],
                 headers: {
                     'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                     'Content-Type': 'application/json'
                 }

             }).done(function() {
                 alert("Subscription deleted")
                 location.reload();
             });
        };

    });

    function disableButtons() {
        $('#delete_subscription').attr('disabled','disabled');
    }


    function enableButtons() {
        $('#delete_subscription').removeAttr('disabled');
    }


    $.get({
        url: 'rest/get_active_customer_subscriptions',
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        $(document).ready(function() {
            var table = $('#subscription_list_id').DataTable();

            $('#subscription_list_id tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                    disableButtons()
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    enableButtons()
                }
            } );

            $('#button').click( function () {
                table.row('.selected').remove().draw( false );
            } );
        } );

        var json = data;
        var dataSet = []
        for(var i = 0; i < json.length; i++){
            var obj = json[i];
            dataSet.push([obj.id, obj.name, obj.details, obj.fee])
        }

        //$("#customer_list_id").html(data);
        $('#subscription_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { "visible": false, title: "Id"},
                    { title: "Name" },
                    { title: "Description" },
                    { title: "Fee" },
                ]
            });
    });
});