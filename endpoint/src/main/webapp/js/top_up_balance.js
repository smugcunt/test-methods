$(document).ready(function(){
    $("#top_up_balance").click(
        function() {
            var amount = $("#amount_id").val();

            // check fields
            if(amount=='') {
                $('input[type="number"]').css("border","2px solid red");
                $('input[type="number"]').css("box-shadow","0 0 3px red");
                alert("amount is empty");
            } else {
                $.post({
                    url: 'rest/top_up_balance/' + $(customer_id).val(),// localStorage.getItem("userId"),
                    headers: {
                        'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "balance":amount,
                    })
                }).done(function(data) {
                    $.redirect('/subscriptions.jsp', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
                });
            }
        }
    );
});