$(document).ready(function(){
    disableButtons();

    $("#subscriptions").click(
      function () {
          $.redirect('/subscriptions.jsp', {'login': localStorage.getItem('login'), 'pass': localStorage.getItem('password'), 'role': localStorage.getItem('role')}, 'GET');
      }
    );

    $("#add_new_subscription").click(
        function() {
            var customerId = localStorage.getItem('id');
            var table = $('#available_sub_list_id').DataTable();
            var rowData = $.map(table.rows('.selected').data(), function (item) {
                return item
            });
            var planId = rowData[0];

            $.post({
                url: 'rest/create_subscription',
                headers: {
                    'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'), //CUSTOMER
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    "customerId": localStorage.getItem('userId'),
                    "planId": planId
                })
            }).done(function(data) {
                $.redirect('/subscriptions.jsp', {'login': localStorage.getItem("login"), 'pass': localStorage.getItem("password"), 'role': localStorage.getItem("role")}, 'GET');
           });

        }
    );

    function disableButtons() {
        $('#add_new_subscription').attr('disabled','disabled');
    }


    function enableButtons() {
        $('#add_new_subscription').removeAttr('disabled');
    }

    $.get({
        url: 'rest/get_all_avail_plans_for_customer/' + localStorage.getItem('userId'),
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup')
        }
    }).done(function(data) {
        $(document).ready(function() {
            var table = $('#available_sub_list_id').DataTable();

            $('#available_sub_list_id tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                    disableButtons()
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    enableButtons()
                }
            } );

            $('#button').click( function () {
                table.row('.selected').remove().draw( false );
            } );
        } );

        var json = data;
        var dataSet = []
        for(var i = 0; i < json.length; i++){
            var obj = json[i];
            dataSet.push([obj.id, obj.name, obj.details, obj.fee])
        }

        //$("#customer_list_id").html(data);
        $('#available_sub_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { "visible": false, title: "Id"},
                    { title: "Name" },
                    { title: "Description" },
                    { title: "Fee" },
                ]
            });
    });

});