<%@ page import="ru.nsu.fit.endpoint.shared.Session" %>
<%@ page import="ru.nsu.fit.endpoint.rest.RestService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add customer</title>
    <title>Subscriptions</title>
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css"/>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.redirect.js"></script>
    <script src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/top_up_balance.js"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="jumbotron">
    <input id="customer_id" name="hidden_input" type="hidden" value="<%=Session.User.Id%>">
    <% Session.UpdateUserData(); %>
    <h2>Your balance: <%=Session.User.Balance%></h2>
    <br>
    <h2>How much?</h2>
    <label>Amount :</label>
    <input type="number" name="amount" id="amount_id">
    <br>
    <input type="button" class="btn btn-dark" name="tpb" id="top_up_balance" value="Add">
</div>
</body>
</html>
